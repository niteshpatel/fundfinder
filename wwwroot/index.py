import os
import time
from mod_python import util
from fundfinder import utils
from fundfinder import config
import sqlalchemy as sa


db = utils.get_db()
metadata = utils.get_metadata()


def index(req):
    # Set a flag to show an alert if we have a new unconsolidated
    # funds from external sources
    
    # Show the criteria management link for poweruser
    update_info = isUpdatedOkay()
    return utils.fill_page(
        'index', {
            'num_citywire_unconsolidated': isCitywireUnconsolidated(),
            'num_hl_unconsolidated': isHLUnconsolidated(),
            'last_checked_info': update_info[0],
            'updated_ok': update_info[1],
            'updated_recent': update_info[2],
            'is_admin': (req.userId == 1)})

def isCitywireUnconsolidated():
    return int(db.execute("""
        SELECT     Count(tblCitywireFund.CitywireFundId) as Count
        FROM         tblCitywireFund
        WHERE     (tblCitywireFund.Consolidated = 0 AND tblCitywireFund.Rating > 0)""").fetchone()['Count'])

def isHLUnconsolidated():
    return int(db.execute("""
        SELECT     Count(tblHLFund.HLFundId) as Count
        FROM         tblHLFund
        WHERE     (tblHLFund.Consolidated = 0)""").fetchone()['Count'])

def isUpdatedOkay():
    # Report if we had any problems updating, and the
    # date of the last successful update
    # Get the last updated data
    names = os.listdir(config.LAST_CHECKED)
    data = []
    aok = True
    recent = True
    # Tell the user if the data (or any of it) is older than  a week
    lastweek = time.strftime('%Y-%m-%d', time.localtime(time.time()-(60*60*24*7)))
    today = time.strftime('%Y-%m-%d')
    for name in names:
        path = os.path.join(config.LAST_CHECKED, name)
        status, date = open(path).read().strip().split(' ')
        if status == 'failed':
            aok = False
        if date < lastweek:
            recent = False
        data.append({
            'name': name[:-4],
            'status': status,
            'recent': date >= lastweek,
            'date': date})
    return data, aok, recent
        