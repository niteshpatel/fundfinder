from mod_python import util
from fundfinder import utils
from fundfinder import config
import sqlalchemy as sa


db = utils.get_db()
metadata = utils.get_metadata()


def index(req):
    table = sa.Table('tblCriteria', metadata, autoload=True)
    rows = table.select(order_by=[table.c.DisplaySequence]).execute().fetchall()
    # Get preferences
    criteria_group_id = int(req.form.getfirst('id').value)
    table = sa.Table('tblCriteriaEntry', metadata, autoload=True)
    prefs = dict([(row['CriteriaId'], row['Requirement']) for row in table.select(table.c.CriteriaGroupId==criteria_group_id).execute().fetchall()])
    data = []
    for row in rows:
        row_data = {
            'CriteriaId': row['CriteriaId'],
            'CriteriaName': row['CriteriaName'],
            'RequirementType': row['RequirementType'],
            }
        row_data['Requirement'] = prefs.get(row['CriteriaId'])
        data.append(row_data)

    # Get the name of the criteria group
    table = sa.Table('tblCriteriaGroup', metadata, autoload=True)
    criteria_group_name = table.select(
        table.c.CriteriaGroupId==criteria_group_id).execute().fetchone()['CriteriaGroupName']
    return utils.fill_page(
        'edit_criteria_group', {
            'data': data,
            'criteria_group_id': criteria_group_id,
            'criteria_group_name': criteria_group_name})

def save(req):
    table = sa.Table('tblCriteria', metadata, autoload=True)
    rows = table.select().execute().fetchall()    
    criteria_group_id = int(req.form.getfirst('criteria_group_id').value)
    # Save preferences
    # Get included criteria
    included = [int(item.value) for item in req.form.getlist('CriteriaId')]
    table = sa.Table('tblCriteriaEntry', metadata, autoload=True)
    table.delete(table.c.CriteriaGroupId==criteria_group_id).execute()
    to_insert = []
    for row in rows:
        if row['CriteriaId'] in included:
            to_insert.append({
                'CriteriaGroupId': criteria_group_id,
                'CriteriaId': int(row['CriteriaId']),
                'Requirement': req.form.getfirst('Requirement_%s' % row['CriteriaId']).value,
                })
    if to_insert:
        table.insert().execute(*to_insert)
    # Save the new table name
    table = sa.Table('tblCriteriaGroup', metadata, autoload=True)
    table.update(
        table.c.CriteriaGroupId==criteria_group_id).execute(
            CriteriaGroupName=req.form.getfirst('criteria_group_name').value)
    # Send the user back to the default view
    util.redirect(req, '/criteria_group_funds.py?id=%s' % criteria_group_id)
    