from mod_python import util
from fundfinder import utils
from fundfinder import config
from criteria_group_funds import get_sorted_fund_points, get_fund_names, get_fund_stats
import sqlalchemy as sa

db = utils.get_db()
metadata = utils.get_metadata()


def index(req):
    # Get the preferences
    model_id = int(req.form.getfirst('id').value)
    table = sa.Table('tblModelCriteriaGroup', metadata, autoload=True)
    criteria_group_ids = [
        row.CriteriaGroupId for row in table.select(
            table.c.ModelId==model_id).execute().fetchall()]
    tblCriteriaEntry = sa.Table('tblCriteriaEntry', metadata, autoload=True)

    # Use the most recent data
    data_date = db.execute('SELECT MAX(DateTimeStamp) FROM tblFundStatistics').fetchone()[0]
    filtered_groups = set()
    scores = {}
    for criteria_group_id in criteria_group_ids:
        criteria_entries = tblCriteriaEntry.select(
            tblCriteriaEntry.c.CriteriaGroupId==criteria_group_id).execute().fetchall()
        # Get funds matching each criteria, use the most recent data
        # Get requirement types
        table = sa.Table('tblCriteria', metadata, autoload=True)
        rows = table.select().execute().fetchall()
        requirement_types = dict([(row['CriteriaId'], row['RequirementType']) for row in rows])
        # Each time we go thru the loop, we AND our filters together
        filtered = None
        for item in criteria_entries:
            # Select clause differs a little for each comparison type
            requirement_type = requirement_types[item['CriteriaId']]
            if requirement_type == '=':
                cmp_result = "1"
                criteria_line = "AND [tblFundStatistics].[CriteriaValue] = '%s'" % item['Requirement']
            else:
                cmp_result = "CONVERT(FLOAT, [tblFundStatistics].[CriteriaValue])"
                criteria_line = "AND CONVERT(FLOAT, [tblFundStatistics].[CriteriaValue]) %s %s" % (requirement_type, item['Requirement'])
            sql = """
                SELECT [tblFundStatistics].[FundId], %s AS Score
                FROM [tblFundStatistics] 
                WHERE [tblFundStatistics].[CriteriaId] = %s
                AND DateTimeStamp = '%s'
                %s
                """ % (cmp_result, item['CriteriaId'], data_date, criteria_line)
            # This gets all funds matching the criteria
            rows = db.execute(sql).fetchall()
            # Update the scores dictionary
            if item['CriteriaId'] in scores:  
                scores[item['CriteriaId']].update(dict([(row['FundId'], row['Score']) for row in rows]))
            else:
                scores[item['CriteriaId']] = dict([(row['FundId'], row['Score']) for row in rows])
            funds = [row['FundId'] for row in rows]
            # Filter the funds more and more.. (geek)
            if filtered is None:
                filtered = set(funds)
            else:
                filtered = filtered.intersection(funds)
        filtered_groups = filtered_groups.union(filtered)

    # Calculate the scores for the funds
    sorted_fund_points = get_sorted_fund_points(scores, filtered_groups)

    # Get fund names
    data = get_fund_names(sorted_fund_points)

    # Get default columns to show and sort order for each criteria
    table = sa.Table('tblCriteria', metadata, autoload=True)
    criteria = table.select().execute().fetchall()
    default_criteria = table.select(table.c.DefaultShown==1, order_by=[table.c.DisplaySequence]).execute().fetchall()

    # Get custom columns or use default
    table = sa.Table('vwModelColumn', metadata, autoload=True)
    columns = table.select(table.c.ModelId==model_id).execute().fetchall()
    if not columns:
        columns = default_criteria

    # Get the data for all the funds
    fund_stats, means, medians = get_fund_stats(filtered_groups, data_date, columns)

    # Get model name
    table = sa.Table('tblModel', metadata, autoload=True)
    model_name = table.select(
        table.c.ModelId==model_id).execute().fetchone()['ModelName']
    
    return utils.fill_page(
        'criteria_group_funds', {
            'data': data,
            'columns': columns,
            'fund_stats': fund_stats,
            'means': means,
            'medians': medians,            
            'model_name': model_name,
            'model_id': model_id,
            'date': data_date.strftime('%d-%b-%Y')})
    