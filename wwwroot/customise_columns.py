from mod_python import util
from fundfinder import utils
from fundfinder import config
import sqlalchemy as sa


db = utils.get_db()
metadata = utils.get_metadata()


def model(req):
    table = sa.Table('tblCriteria', metadata, autoload=True)
    rows = table.select(order_by=[table.c.DisplaySequence]).execute().fetchall()
    # Get preferences
    model_id = int(req.form.getfirst('id').value)
    table = sa.Table('tblModelColumn', metadata, autoload=True)
    columns = [row['CriteriaId'] for row in table.select(
        table.c.ModelId==model_id).execute().fetchall()]
    # Get model name
    table = sa.Table('tblModel', metadata, autoload=True)
    model_name = table.select(
        table.c.ModelId==model_id).execute().fetchone()['ModelName']    
    return utils.fill_page(
        'customise_columns', {
            'model_id': model_id,
            'model_name': model_name,
            'columns': columns,
            'criteria': rows})

def criteria_group(req):
    table = sa.Table('tblCriteria', metadata, autoload=True)
    rows = table.select(order_by=[table.c.DisplaySequence]).execute().fetchall()
    # Get preferences
    criteria_group_id = int(req.form.getfirst('id').value)
    table = sa.Table('vwCriteriaGroupColumn', metadata, autoload=True)
    columns = [row['CriteriaId'] for row in table.select(
        table.c.CriteriaGroupId==criteria_group_id).execute().fetchall()]
    # Get criteria group name
    table = sa.Table('tblCriteriaGroup', metadata, autoload=True)
    criteria_group_name = table.select(
        table.c.CriteriaGroupId==criteria_group_id).execute().fetchone()['CriteriaGroupName']    
    return utils.fill_page(
        'customise_columns', {
            'criteria_group_id': criteria_group_id,
            'criteria_group_name': criteria_group_name,
            'columns': columns,
            'criteria': rows})

def save_criteria_group(req):
    table = sa.Table('tblCriteria', metadata, autoload=True)
    rows = table.select().execute().fetchall()
    criteria_group_id = int(req.form.getfirst('criteria_group_id').value)
    # Save the users choice of columns
    table = sa.Table('tblCriteriaGroupColumn', metadata, autoload=True)
    table.delete(table.c.CriteriaGroupId==criteria_group_id).execute()
    to_insert = []
    for criteriaId in [row['CriteriaId'] for row in rows]:
        if getattr(req.form.getfirst('Shown_%s' % criteriaId), 'value', None):
            to_insert.append({
                'CriteriaGroupId': criteria_group_id,
                'CriteriaId': int(criteriaId)})
    if to_insert:
        table.insert().execute(*to_insert)
    util.redirect(req, '/criteria_group_funds.py?id=%s' % criteria_group_id)

def save_model(req):
    table = sa.Table('tblCriteria', metadata, autoload=True)
    rows = table.select().execute().fetchall()
    model_id = int(req.form.getfirst('model_id').value)
    # Save the users choice of columns
    table = sa.Table('tblModelColumn', metadata, autoload=True)
    table.delete(table.c.ModelId==model_id).execute()
    to_insert = []
    for criteriaId in [row['CriteriaId'] for row in rows]:
        if getattr(req.form.getfirst('Shown_%s' % criteriaId), 'value', None):
            to_insert.append({
                'ModelId': model_id,
                'CriteriaId': int(criteriaId)})
    if to_insert:
        table.insert().execute(*to_insert)
    util.redirect(req, '/model_funds.py?id=%s' % model_id)
