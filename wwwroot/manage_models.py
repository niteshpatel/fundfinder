from mod_python import util
from fundfinder import utils
from fundfinder import config
import sqlalchemy as sa


db = utils.get_db()
metadata = utils.get_metadata()


def index(req):
    table = sa.Table('tblModel', metadata, autoload=True)
    rows = table.select(table.c.UserId==req.userId).execute().fetchall()
    return utils.fill_page(
        'manage_models', {
            'models': rows})

def create(req):
    table = sa.Table('tblModel', metadata, autoload=True)
    table.insert().execute({
        'ModelName': 'new_model',
        'UserId': req.userId})
    new_id = db.execute('SELECT MAX(ModelId) FROM tblModel').fetchone()[0]
    # Send the user back to the edit page
    util.redirect(req, '/edit_model.py?id=%s' % new_id)
    
def delete(req):
    # Get all the rows, and update them using form data
    table = sa.Table('tblModel', metadata, autoload=True)
    linked1 = sa.Table('tblModelCriteriaGroup', metadata, autoload=True)
    linked2 = sa.Table('tblModelColumn', metadata, autoload=True)
    for item in table.select().execute():
        if req.form.getfirst('ModelId_%i' % item.ModelId):
            # We need to delete, delete children first
            linked1.delete(linked1.c.ModelId==item.ModelId).execute()
            linked2.delete(linked2.c.ModelId==item.ModelId).execute()
            table.delete(table.c.ModelId==item.ModelId).execute()
            
    # Send the user back to the default view
    util.redirect(req, '/manage_models.py')
