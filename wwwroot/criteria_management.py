from mod_python import util
from fundfinder import utils
from fundfinder import config
import sqlalchemy as sa


db = utils.get_db()
metadata = utils.get_metadata()


def index(req):
    table = sa.Table('tblCriteria', metadata, autoload=True)
    rows = table.select(order_by=[table.c.DisplaySequence]).execute().fetchall()
    return utils.fill_page(
        'criteria_management', {
            'criteria': rows})

def save(req):
    table = sa.Table('tblCriteria', metadata, autoload=True)
    # If I don't put these lines in sqlalchemy starts crying
    sa.Table('tblCriteria', metadata, autoload=True)
    sa.orm.clear_mappers()
    sa.orm.mapper(Criteria, table)
    session = sa.orm.create_session()
    query = session.query(Criteria)
    # Get all the rows, and update them using form data
    rows = query.select()
    for item in rows:
        item.Points = int(req.form.getfirst('Points_%i' % item.CriteriaId).value)
        item.DefaultShown = getattr(req.form.getfirst('DefaultShown_%s' % item.CriteriaId), 'value', 0)
        requirement_type = req.form.getfirst('RequirementType_%i' % item.CriteriaId).value
        if requirement_type not in ('=', '<', '>'):
            raise ValueError
        item.RequirementType = requirement_type
    session.flush()
    # Send the user back to the default view
    util.redirect(req, '/criteria_management.py')


class Criteria(object):
    pass
