from mod_python import util
from fundfinder import utils
from fundfinder import config
import sqlalchemy as sa


db = utils.get_db()
metadata = utils.get_metadata()


def index(req):
    table = sa.Table('tblCriteriaGroup', metadata, autoload=True)
    rows = table.select(table.c.UserId==req.userId).execute().fetchall()
    return utils.fill_page(
        'manage_criteria_groups', {
            'criteria_groups': rows})

def create(req):
    table = sa.Table('tblCriteriaGroup', metadata, autoload=True)
    table.insert().execute({
        'CriteriaGroupName': 'new_criteria_group',
        'UserId': req.userId})
    new_id = db.execute('SELECT MAX(CriteriaGroupId) FROM tblCriteriaGroup').fetchone()[0]
    # Send the user back to the edit page
    util.redirect(req, '/edit_criteria_group.py?id=%s' % new_id)
    
def delete(req):
    # Get all the rows, and update them using form data
    table = sa.Table('tblCriteriaGroup', metadata, autoload=True)
    linked1 = sa.Table('tblCriteriaEntry', metadata, autoload=True)
    linked2 = sa.Table('tblCriteriaGroupColumn', metadata, autoload=True)
    for item in table.select(table.c.UserId==req.userId).execute():
        if req.form.getfirst('CriteriaGroupId_%i' % item.CriteriaGroupId):
            # We need to delete, delete children first
            linked1.delete(linked1.c.CriteriaGroupId==item.CriteriaGroupId).execute()            
            linked2.delete(linked2.c.CriteriaGroupId==item.CriteriaGroupId).execute()
            table.delete(table.c.CriteriaGroupId==item.CriteriaGroupId).execute()
            
    # Send the user back to the default view
    util.redirect(req, '/manage_criteria_groups.py')
