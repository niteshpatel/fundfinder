from mod_python import util
from fundfinder import utils
from fundfinder import config
import sqlalchemy as sa


db = utils.get_db()
metadata = utils.get_metadata()


def index(req):
    table = sa.Table('tblCriteriaGroup', metadata, autoload=True)
    criteria_groups = table.select(table.c.UserId==req.userId).execute().fetchall()
    # Get the preferences
    model_id = int(req.form.getfirst('id').value)
    table = sa.Table('tblModelCriteriaGroup', metadata, autoload=True)
    model_criteria_groups = table.select(
        table.c.ModelId==model_id).execute().fetchall()
    preferences = dict([(row['CriteriaGroupId'], None) for row in model_criteria_groups])
    data = []
    for row in criteria_groups:
        included = preferences.has_key(row['CriteriaGroupId'])
        data.append({
            'CriteriaGroupId': row['CriteriaGroupId'],
            'CriteriaGroupName': row['CriteriaGroupName'],
            'Included': included})
    # Get the name of the criteria group
    table = sa.Table('tblModel', metadata, autoload=True)
    model_name = table.select(
        table.c.ModelId==model_id).execute().fetchone()['ModelName']
    return utils.fill_page(
        'edit_model', {
            'data': data,
            'model_id': model_id,
            'model_name': model_name})

def save(req):
    model_id = int(req.form.getfirst('model_id').value)
    # Delete existing entries
    table = sa.Table('tblModelCriteriaGroup', metadata, autoload=True)
    table.delete(table.c.ModelId==model_id).execute()
    # Now put the new values in
    selected = req.form.getlist('CriteriaGroupId')
    to_insert = []
    for item in selected:
        to_insert.append({
            'ModelId': model_id,
            'CriteriaGroupId': int(item.value)})
    if to_insert:
        table.insert().execute(*to_insert)
    # Save the new table name
    table = sa.Table('tblModel', metadata, autoload=True)
    table.update(
        table.c.ModelId==model_id).execute(
            ModelName=req.form.getfirst('model_name').value)
    # Send the user back to the default view
    util.redirect(req, '/model_funds.py?id=%s' % model_id)
    