from mod_python import util
import sqlalchemy as sa

from fundfinder import utils
from fundfinder import config


db = utils.get_db()
metadata = utils.get_metadata()


def fund_data(criteria_entries, requirement_types, data_date):
    # Each time we go thru the loop, we AND our filters together
    scores = {}
    filtered = None
    for item in criteria_entries:
        # Select clause differs a little for each comparison type
        requirement_type = requirement_types[item['CriteriaId']]
        if requirement_type == '=':
            cmp_result = "1"
            criteria_line = "AND [tblFundStatistics].[CriteriaValue] = '%s'" % item['Requirement']
        else:
            cmp_result = "CONVERT(FLOAT, [tblFundStatistics].[CriteriaValue])"
            criteria_line = "AND CONVERT(FLOAT, [tblFundStatistics].[CriteriaValue]) %s %s" % (requirement_type, item['Requirement'])
        sql = """
            SELECT [tblFundStatistics].[FundId], %s AS Score
            FROM [tblFundStatistics] 
            WHERE [tblFundStatistics].[CriteriaId] = %s
            AND DateTimeStamp = %s
            %s
            """ % (cmp_result, item['CriteriaId'], data_date == None and 'NULL' or '\'%s\'' % data_date, criteria_line)
        rows = db.execute(sql).fetchall()
        scores[item['CriteriaId']] = dict([(row['FundId'], row['Score']) for row in rows])
        funds = [row['FundId'] for row in rows]
        if filtered is None:
            filtered = set(funds)
        else:
            filtered = filtered.intersection(funds)

    # Calculate the scores for the funds
    sorted_fund_points = get_sorted_fund_points(scores, filtered)
    
    # Get fund names
    data = get_fund_names(sorted_fund_points)
    return filtered, data
    
    
def index(req):
    # Get the preferences
    criteria_group_id = int(req.form.getfirst('id').value)
    table = sa.Table('tblCriteriaEntry', metadata, autoload=True)
    criteria_entries = table.select(
        table.c.CriteriaGroupId==criteria_group_id).execute().fetchall()

    # Get requirement types
    table = sa.Table('tblCriteria', metadata, autoload=True)
    rows = table.select().execute().fetchall()
    requirement_types = dict([(row['CriteriaId'], row['RequirementType']) for row in rows])    

    # Get sort order for each criteria
    table = sa.Table('tblCriteria', metadata, autoload=True)
    criteria = table.select().execute().fetchall()
    default_criteria = table.select(table.c.DefaultShown==1, order_by=[table.c.DisplaySequence]).execute().fetchall()

    # Get criteria group name
    table = sa.Table('tblCriteriaGroup', metadata, autoload=True)
    criteria_group_name = table.select(
        table.c.CriteriaGroupId==criteria_group_id).execute().fetchone()['CriteriaGroupName']
    
    # Get custom columns or use default
    table = sa.Table('vwCriteriaGroupColumn', metadata, autoload=True)
    columns = table.select(table.c.CriteriaGroupId==criteria_group_id).execute().fetchall()
    if not columns:
        columns = default_criteria
    criteria_data = dict([(item['CriteriaId'], item) for item in criteria])

    # Get funds matching each criteria, use the most recent data
    data_date = db.execute('SELECT MAX(DateTimeStamp) FROM tblFundStatistics').fetchone()[0]
    date_date_6m = db.execute('SELECT MAX(DateTimeStamp) FROM tblFundStatistics WHERE (DateTimeStamp < DATEADD(m, - 6, { fn NOW() }))').fetchone()[0]
    date_date_6m = None and 'Null'

    # Get the data for all the funds
    filtered, data = fund_data(criteria_entries, requirement_types, data_date)
    fund_stats, means, medians = get_fund_stats(filtered, data_date, columns)
    
    filtered_6m, data_6m = fund_data(criteria_entries, requirement_types, date_date_6m)
    fund_stats_6m, means_6m, medians_6m = get_fund_stats(filtered_6m, date_date_6m, columns)    
    
    return utils.fill_page(
        'criteria_group_funds', {
            'columns': columns,
            'data': data,
            'fund_stats': fund_stats,
            'means': means,
            'medians': medians,
            'data_6m': data_6m,
            'fund_stats_6m': fund_stats_6m,
            'means_6m': means_6m,
            'medians_6m': medians_6m,
            'criteria_group_id': criteria_group_id,
            'criteria_group_name': criteria_group_name,
            'date': data_date.strftime('%d-%b-%Y')})

            
def get_sorted_fund_points(scores, filtered):
    table = sa.Table('tblCriteria', metadata, autoload=True)
    rows = table.select().execute().fetchall()
    fund_points = {}
    for row in rows:
        criteria_id = row['CriteriaId']
        points = row['Points']
        fund_set = scores.get(criteria_id, {})
        for fund_id, score in fund_set.items():
            # Dont include funds not in our filtered list
            if fund_id not in filtered:
                continue
            rating = score * points
            if fund_id in fund_points:
                fund_points[fund_id] += rating
            else:
                fund_points[fund_id] = rating
    return sorted([(value, key) for key, value in fund_points.items()], reverse=True)

def get_fund_names(sorted_fund_points):
    table = sa.Table('tblFund', metadata, autoload=True)
    id_to_name = dict([(row['FundId'], row['FundName']) for row in table.select().execute().fetchall()])
    data = []
    for points, fund_id in sorted_fund_points:
        data.append({'FundId': fund_id, 'Points': points, 'FundName': id_to_name[fund_id]})
    return data

    
def get_fund_stats(filtered, data_date, columns):
    fund_stats = {}
    if filtered:
        sql = """
            SELECT [tblFundStatistics].*
            FROM [tblFundStatistics] 
            WHERE [tblFundStatistics].[FundId] in (%s)
            AND DateTimeStamp = '%s'
            """ % (
                ','.join(map(lambda s: ("'%s'" % s.replace("'", "''")), filtered)),
                data_date)
        stats = db.execute(sql).fetchall()
        fund_stats = {}
        for row in stats:
            if row['FundId'] not in fund_stats:
                fund_stats[row['FundId']] = [row]
            else:
                fund_stats[row['FundId']].append(row)
        for fund in fund_stats:
            fund_stats[fund] = dict([(item['CriteriaId'], item['CriteriaValue']) for item in fund_stats[fund]])

    # Calculate averages
    analysis = {}
    for col in columns:
        criteriaId = col['CriteriaId']
        if col['RequirementType'] != '=':
            analysis[criteriaId] = []
            for fund in fund_stats:
                if criteriaId in fund_stats[fund]:
                    try:
                        analysis[criteriaId].append(float(fund_stats[fund][criteriaId]))
                    except:
                        raise str(criteriaId) + fund_stats[fund][criteriaId]
    means = {}
    medians = {}
    for item, values in analysis.items():
        means[item] = medians[item] = 0
        if len(values):            
            means[item] = '%.1f' % (sum(values)/len(values))
            medians[item] = '%.1f' % (sorted(values)[int(len(values)/2)])
    return fund_stats, means, medians