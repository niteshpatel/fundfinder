if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tblFund_tblCitywireFund]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tblFund] DROP CONSTRAINT FK_tblFund_tblCitywireFund
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tblCriteriaEntry_tblCriteria]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tblCriteriaEntry] DROP CONSTRAINT FK_tblCriteriaEntry_tblCriteria
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tblCriteriaGroupColumn_tblCriteria]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tblCriteriaGroupColumn] DROP CONSTRAINT FK_tblCriteriaGroupColumn_tblCriteria
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tblFundStatistics_tblCriteria]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tblFundStatistics] DROP CONSTRAINT FK_tblFundStatistics_tblCriteria
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tblModelColumn_tblCriteria]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tblModelColumn] DROP CONSTRAINT FK_tblModelColumn_tblCriteria
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tblCriteriaEntry_tblCriteriaGroup]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tblCriteriaEntry] DROP CONSTRAINT FK_tblCriteriaEntry_tblCriteriaGroup
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tblCriteriaGroupColumn_tblCriteriaGroup]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tblCriteriaGroupColumn] DROP CONSTRAINT FK_tblCriteriaGroupColumn_tblCriteriaGroup
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tblModelCriteriaGroup_tblCriteriaGroup]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tblModelCriteriaGroup] DROP CONSTRAINT FK_tblModelCriteriaGroup_tblCriteriaGroup
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tblFundStatistics_tblFund]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tblFundStatistics] DROP CONSTRAINT FK_tblFundStatistics_tblFund
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tblFund_tblHLFund]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tblFund] DROP CONSTRAINT FK_tblFund_tblHLFund
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tblModelColumn_tblModel]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tblModelColumn] DROP CONSTRAINT FK_tblModelColumn_tblModel
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tblModelCriteriaGroup_tblModel]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tblModelCriteriaGroup] DROP CONSTRAINT FK_tblModelCriteriaGroup_tblModel
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tblCriteriaGroup_tblUser]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tblCriteriaGroup] DROP CONSTRAINT FK_tblCriteriaGroup_tblUser
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tblModel_tblUser]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tblModel] DROP CONSTRAINT FK_tblModel_tblUser
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vwCriteriaEntry]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vwCriteriaEntry]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vwCriteriaGroupColumn]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vwCriteriaGroupColumn]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vwModelColumn]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vwModelColumn]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vwFund]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vwFund]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vwCitywireUnconsolidated]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vwCitywireUnconsolidated]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tblCitywireFund]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tblCitywireFund]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tblCriteria]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tblCriteria]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tblCriteriaEntry]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tblCriteriaEntry]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tblCriteriaGroup]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tblCriteriaGroup]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tblCriteriaGroupColumn]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tblCriteriaGroupColumn]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tblFund]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tblFund]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tblFundStatistics]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tblFundStatistics]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tblHLFund]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tblHLFund]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tblModel]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tblModel]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tblModelColumn]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tblModelColumn]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tblModelCriteriaGroup]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tblModelCriteriaGroup]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tblUser]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tblUser]
GO

CREATE TABLE [dbo].[tblCitywireFund] (
	[CitywireFundId] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL ,
	[CitywireFundName] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL ,
	[Manager] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL ,
	[Rating] [int] NOT NULL ,
	[Consolidated] [tinyint] NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[tblCriteria] (
	[CriteriaId] [int] IDENTITY (1, 1) NOT NULL ,
	[CriteriaName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL ,
	[ShortName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL ,
	[Points] [int] NOT NULL ,
	[RequirementType] [char] (1) COLLATE Latin1_General_CI_AS NOT NULL ,
	[DefaultValue] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL ,
	[DefaultShown] [tinyint] NOT NULL ,
	[DefaultSort] [tinyint] NOT NULL ,
	[DisplaySequence] [float] NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[tblCriteriaEntry] (
	[CriteriaGroupId] [int] NOT NULL ,
	[CriteriaId] [int] NOT NULL ,
	[Requirement] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[tblCriteriaGroup] (
	[CriteriaGroupId] [int] IDENTITY (1, 1) NOT NULL ,
	[CriteriaGroupName] [varchar] (200) COLLATE Latin1_General_CI_AS NOT NULL ,
	[UserId] [int] NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[tblCriteriaGroupColumn] (
	[CriteriaGroupId] [int] NOT NULL ,
	[CriteriaId] [int] NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[tblFund] (
	[FundId] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL ,
	[FundName] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL ,
	[CitywireFundId] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL ,
	[HLFundId] [varchar] (100) COLLATE Latin1_General_CI_AS NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[tblFundStatistics] (
	[FundId] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL ,
	[CriteriaId] [int] NOT NULL ,
	[DateTimeStamp] [datetime] NOT NULL ,
	[CriteriaValue] [varchar] (50) COLLATE Latin1_General_CI_AS NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[tblHLFund] (
	[HLFundId] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL ,
	[HLFundName] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL ,
	[Active] [tinyint] NOT NULL ,
	[Consolidated] [tinyint] NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[tblModel] (
	[ModelId] [int] IDENTITY (1, 1) NOT NULL ,
	[ModelName] [varchar] (200) COLLATE Latin1_General_CI_AS NOT NULL ,
	[UserId] [int] NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[tblModelColumn] (
	[ModelId] [int] NOT NULL ,
	[CriteriaId] [int] NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[tblModelCriteriaGroup] (
	[ModelId] [int] NOT NULL ,
	[CriteriaGroupId] [int] NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[tblUser] (
	[UserId] [int] IDENTITY (1, 1) NOT NULL ,
	[UserName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL ,
	[Password] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL 
) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.vwCitywireUnconsolidated
AS
SELECT     TOP 100 PERCENT dbo.tblCitywireFund.*
FROM         dbo.tblCitywireFund
WHERE     (Consolidated = 0) AND (Rating > 0)
ORDER BY CitywireFundName

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.vwFund
AS
SELECT     TOP 100 PERCENT dbo.tblFund.*
FROM         dbo.tblFund
ORDER BY FundName

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.vwCriteriaEntry
AS
SELECT     TOP 100 PERCENT dbo.tblCriteriaEntry.CriteriaGroupId, dbo.tblCriteriaEntry.CriteriaId AS Include, dbo.tblCriteria.CriteriaId, dbo.tblCriteria.CriteriaName, 
                      dbo.tblCriteria.RequirementType, dbo.tblCriteriaEntry.Requirement
FROM         dbo.tblCriteria LEFT OUTER JOIN
                      dbo.tblCriteriaEntry ON dbo.tblCriteria.CriteriaId = dbo.tblCriteriaEntry.CriteriaId
ORDER BY dbo.tblCriteria.DisplaySequence

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.vwCriteriaGroupColumn
AS
SELECT     TOP 100 PERCENT dbo.tblCriteriaGroupColumn.CriteriaGroupId, dbo.tblCriteria.*
FROM         dbo.tblCriteria INNER JOIN
                      dbo.tblCriteriaGroupColumn ON dbo.tblCriteria.CriteriaId = dbo.tblCriteriaGroupColumn.CriteriaId
ORDER BY dbo.tblCriteria.DisplaySequence

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.vwModelColumn
AS
SELECT     TOP 100 PERCENT dbo.tblModelColumn.ModelId, dbo.tblCriteria.*
FROM         dbo.tblModelColumn INNER JOIN
                      dbo.tblCriteria ON dbo.tblModelColumn.CriteriaId = dbo.tblCriteria.CriteriaId
ORDER BY dbo.tblCriteria.DisplaySequence

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

