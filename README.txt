Prerequisites
-------------
1) Apache 2.2.25 (tested with httpd-2.2.25-win32-x86-openssl-0.9.8y.msi)
2) Python 2.5 (tested with python-2.5.4.msi)
3) mod_python 3.3 (tested with mod_python-3.3.1.win32-py2.5-Apache2.2.exe)
4) np python package from https://bitbucket.org/niteshpatel/python-packages
5) SimpleTAL 4.3 (tested with SimpleTAL-4.3.tar.gz, https://www.owlfish.com/software/simpleTAL)
6) SQLAlchemy 0.5 (tested with SQLAlchemy-0.5.8.tar.gz, https://pypi.python.org/pypi/SQLAlchemy/0.5.8)
7) BeautifulSoup 3.2 (tested with BeautifulSoup-3.2.1.tar.gz, https://pypi.python.org/pypi/BeautifulSoup)
8) pyodbc 3.0 (tested with pyodbc-3.0.6.win32-py2.5.exe, https://code.google.com/p/pyodbc/)
9) SQL Server (tested with en_sql_server_2008_r2_standard_x86_x64_ia64_dvd_521546.iso)

Installation
------------
1) Clone the repo to a local folder
2) Add apacheconf/funds.no-ip.com.conf to Apache includes
3) Update apacheconf/funds.no-ip.com.conf with environment specific info
4) Add the full path to pyfiles to a pth file on the Python path
5) Add entry 127.0.0.1 funds.no-ip.com to HOSTS file
6) Update pyfiles/fundfinder/config.py with environment specific info
7) Create database FundFinder 
8) Create user 'fundfinder' with password 'password' with db_owner on FundFinder
9) Run scripts/FundFinder.sql against the FundFinder database
10) Add UserName 'user' and password 'user' to tblUser to create a test user
11) Import data from scripts/tblCriteria.csv into FundFinder database tblCriteria
12) Create scheduled task to run scripts/updatedata.bat to update Fund stats
