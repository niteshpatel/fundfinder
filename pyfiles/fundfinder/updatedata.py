import cPickle
import os
import re
import sets
import sqlalchemy as sa
import time
import urllib

from fundfinder import config
from fundfinder import screenscraper


db = sa.create_engine(config.CONSTRING, auto_identity_insert=False)
metadata = sa.MetaData(db)


NUM = '-?[0-9,]*\.?[0-9,]*(?:n/a)?'
TEXT = '[\.\(\)\s\w/&;-]+'


class DataSourceBase:
    
    SOUP_INFO = None
    COOKIE_INFO = None

    def get_raw_pages(self):
        ss = screenscraper.ScreenScraper(self.BASE_URL, self.NUM_PAGES, self.SOUP_INFO, self.COOKIE_INFO)
        for i in range(ss.num_pages):
            print 'Getting data for page %i/%i...' % (i+1, ss.num_pages), 
            ss.next()
            print 'DONE'
            print
        print 'DONE All pages'
        print
        return ss.pages.values()
        

def setUpdateStatus(inst, status):
    # Determine file we need to write to
    name = '%s.txt' % inst.__class__.__name__
    path = os.path.join(config.LAST_CHECKED, name)
    # Get todays date
    date = time.strftime('%Y-%m-%d')
    # write the file saying if we were successfull or not
    open(path, 'w').write('%s %s' %(status, date))
    
    
def getData(data, instructions):
    # Get the raw html from the url
    #time.sleep(0.8)
    body = data
    # Now follow the instructions
    results = {}

    for item in instructions:
        if item.get('multiple'):
        
            # import pdb
            # pdb.set_trace()
            
            matches = [match.groupdict() for match in re.finditer(item['regex'], body)]
            # Apply defaults
            if item.get('defaults'):
                for match in matches:
                    for key, value in item['defaults'].items():
                        if not match.get(key) or match[key] == 'n/a':
                            match[key] = value
            # Apply int conversions
            if item.get('convert'):
                for match in matches:
                    for key, value in item['convert'].items():
                        match[key] = value(match[key])
        else:
            matches = re.search(item['regex'], body)
            if not matches:
                matches = {}
            else:
                matches = matches.groupdict()
            if item.get('defaults'):
                for key, value in item['defaults'].items():
                    if not matches.get(key) or matches.get(key) == 'n/a':
                        matches[key] = value
            # Apply int conversions
            if item.get('convert'):
                for key, value in item['convert'].items():
                    matches[key] = value(matches[key])
        results[item['key']] = matches
    return results


class TrustNet(DataSourceBase):

    SOUP_INFO = {'data_element': {'name': 'table', 'attrs': {'summary': 'Fund prices'}}}

    def doWork(self):
        # Write status file
        setUpdateStatus(self, 'failed')

        pages = self.get_raw_pages()
        
        # Parse the results page to get the fund names
        foundData = False
        for data in pages:
            funds = getData(data, [{
                'key': 'funds',
                'regex': self.FUND_RE,
                'defaults': {
                    'riskgrade': 999, 
                    'm1': 0, 
                    'm3': 0, 
                    'm6': 0, 
                    'y1': 0, 
                    'y2': 0, 
                    'y3': 0, 
                    'y5': 0,                     
                    'y10': 0, 
                    'm12to24': 0,
                    'm24to36': 0,
                    'm36to48': 0,
                    'm48to60': 0,
                    'id': 0, 
                    'm1n': 0, 
                    'm3n': 0, 
                    'm6n': 0, 
                    'y1n': 0, 
                    'y3n': 0, 
                    'y5n': 0, 
                    'yield': 0,
                    'sandp': '',
                    'obsr': '',
                    'fecrown': 0,
                    'discount': '0'},
                'convert': {
                    'riskgrade': float, 
                    'm1': float, 
                    'm3': float, 
                    'm6': float, 
                    'y1': float, 
                    'y2': float, 
                    'y3': float, 
                    'y5': float, 
                    'y10': float, 
                    'm12to24': float,
                    'm24to36': float,
                    'm36to48': float,
                    'm48to60': float,
                    'id': str, 
                    'm1n': float, 
                    'm3n': float, 
                    'm6n': float, 
                    'y1n': float, 
                    'y3n': float, 
                    'y5n': float, 
                    'yield': float,
                    'sandp': lambda s: s.count('A'),
                    'obsr': lambda s: s.count('A'),
                    'discount': lambda s: float(s.replace(',', ''))},
                'multiple': True}])['funds']
            if len(funds) > 1:
                foundData = True
            [self.criteria(fund) for fund in funds]
        if not foundData:
            raise ValueError

        # Write status file
        setUpdateStatus(self, 'successful')
            
            
class TrustNet_ALL(TrustNet):
    
    BASE_URL = 'http://www.trustnet.com/Investments/Perf.aspx?univ=U&Pf_AssetClass=A%%3a&Pf_sortedColumn=Performance[Cur].P3m,NameFull&Pf_sortedDirection=DESC&Pf_PageNo=%s'
    COOKIE_INFO = {
        'Pf_CC_U_pricesandperformance': 'Rank,UnitsFlip,Chart,FN,GP,FC,PR,PR,Yield,OBSR,CRR,P1m,P3m,P6m,P12m,P24m,P36m,P60m,P120m,ShortList',
        'Pf_CCP_U_pricesandperformance': 'Rank,UnitsFlip,Chart,FN,GP,FC,PR,PR,Yield,OBSR,CRR,P1m,P3m,P6m,P12m,P24m,P36m,P60m,P120m,ShortList'}
        
    NUM_PAGES = 34

    fundReText = """<tr id="%(TEXT)s" class="%(TEXT)s">%(TEXT)s<td class="Center">%(NUM)s</td><td class="Center"><a id="Id_%(TEXT)s" href="javascript:ShowUnits\(this,'%(TEXT)s','U','Pf','U','%(TEXT)s'\);" title="Expand Units">\[\+\]</a></td><td class="Center"><a href="/Factsheets/Factsheet.aspx\?fundCode=(?P<id>%(TEXT)s)&amp;univ=U&amp;pagetype=performance&amp;typeCode=%(TEXT)s" class="CustomChartIcon" alt="chart"></a></td><td class=""><a href="/Factsheets/Factsheet.aspx\?fundCode=%(TEXT)s&amp;univ=U">(?P<name>%(TEXT)s)</a></td><td class="Left"><a href="/Factsheets/GroupFactSheet.aspx\?managerCode=%(TEXT)s&amp;univ=U">%(TEXT)s</a></td><td class="" align="center"><a class="%(TEXT)s" title="%(TEXT)s"></a></td><td class="" align="center"><div id="%(TEXT)s" class="%(TEXT)s" onclick="javascript:OpenRSChart\('%(TEXT)s','%(TEXT)s','%(TEXT)s',this.id,'%(TEXT)s'\);"><b>%(TEXT)s</b></div></td>(?:<td class="Colspan2" colspan="2">%(NUM)s(?:<br />\(%(TEXT)s\))?</td>|<td class="Center">%(NUM)s</td><td class="Center">%(NUM)s</td>)<td class="Center">(?P<yield>%(NUM)s)</td><td class="Left">(?P<obsr>%(TEXT)s|<span class="obsr_blank_icon"></span>)</td><td class="Center">(?:<a class="FCRcrown%(NUM)s" title="(?P<fecrown>%(NUM)s)"></a>|%(TEXT)s)</td><td class="noWrapCellRight">(?P<m1>%(NUM)s)</td><td class="noWrapCellRight">(?P<m3>%(NUM)s)</td><td class="noWrapCellRight">(?P<m6>%(NUM)s)</td><td class="noWrapCellRight">(?P<y1>%(NUM)s)</td><td class="noWrapCellRight">(?P<y2>%(NUM)s)</td><td class="noWrapCellRight">(?P<y3>%(NUM)s)</td><td class="noWrapCellRight">(?P<y5>%(NUM)s)</td><td class="noWrapCellRight">(?P<y10>%(NUM)s)</td><td class="Center"><a href="javascript\:AddFundtoNewBasket\('%(TEXT)s',\'U\',true\);" class="basket_add_icon"></a></td>%(TEXT)s</tr>""" % {'NUM':NUM, 'TEXT':TEXT}
    
    FUND_RE = re.compile(fundReText, re.DOTALL)
    
    def criteria(self, fundMatch):
        criteria_id2value = {
            'm1': 2,
            'm3': 3,
            'm6': 4,
            'y1': 5,
            'y2': 42,
            'y3': 6,
            'y5': 7,
            'y10': 44,
            'yield': 45,
            'sandp': 46,
            'obsr': 47,
            'fecrown': 49,
            'y1volatility': 8,
            'y1sharpe': 9,
            'y1alpha': 10,
            'y1beta': 12,
            'y1info': 13,
            'y1r': 14,
            'y3volatility': 28,
            'y3sharpe': 29,
            'y3alpha': 30,
            'y3beta': 31,
            'y3info': 32,
            'y3r': 33,
            'region': 17,
            'sector': 16,            
            'IMAsector': 38,
            'riskgrade': 27,
            'AFIaggressive': 34,
            'AFIbalanced': 35,
            'AFIcautious': 36,
            }
        # Update or insert fund name
        fund_id = fundMatch['id']
        fund_name = fundMatch['name'].replace('&amp;', '&')
        table = sa.Table('tblFund', metadata, autoload=True)
        if not table.select(table.c.FundId==fund_id).execute().fetchall():
            table.insert().execute(FundId=fund_id, FundName=fund_name)
        else:
            table.update(table.c.FundId==fund_id).execute(FundName=fund_name)
        # Delete existing entries
        today = time.strftime('%d-%b-%Y')        
        table = sa.Table('tblFundStatistics', metadata, autoload=True)
        if not table.select(sa.and_(table.c.DateTimeStamp==today,table.c.FundId==fund_id)).execute().fetchall():
            to_insert = []
            for name, value in criteria_id2value.items():
                if name in fundMatch:
                    to_insert.append({
                        'DateTimeStamp': today,
                        'FundId': fund_id,
                        'CriteriaId': int(value),
                        'CriteriaValue': fundMatch[name]})
            if to_insert:
                table.insert().execute(*to_insert)
        return False


class TrustNet_IT(TrustNet):

    BASE_URL = 'http://www.trustnet.com/Investments/Perf.aspx?univ=T&Pf_AssetClass=A:&Pf_sortedColumn=Performance[Cur].P3m,NameFull&Pf_sortedDirection=DESC&Pf_PageNo=%s'
    COOKIE_INFO = {
        'Pf_CC_T_pricesandperformance': 'Rank,UnitsFlip,Chart,FN,GP,FC,PR,Disc,NAV,Yield,CRR,P1m,P3m,P6m,P12m,P24m,P36m,P60m,P120m,P12t24m,P24t36m,P36t48m,P48t60m,NP1m,NP3m,NP6m,NP12m,NP36m,NP60m,ShortList',
        'Pf_CCP_T_pricesandperformance': 'Rank,UnitsFlip,Chart,FN,GP,FC,PR,Disc,NAV,Yield,CRR,P1m,P3m,P6m,P12m,P24m,P36m,P60m,P120m,P12t24m,P24t36m,P36t48m,P48t60m,NP1m,NP3m,NP6m,NP12m,NP36m,NP60m,ShortList'}
    NUM_PAGES = 8

    fundReText = """<tr id="%(TEXT)s" class="%(TEXT)s">%(TEXT)s<td class="Center">%(NUM)s</td><td class="Center"><a id="Id_%(TEXT)s" href="javascript:ShowUnits\(this,'%(TEXT)s','T','Pf','T','%(TEXT)s'\);" title="Expand Units">\[\+\]</a></td><td class="Center"><a href="/Factsheets/Factsheet.aspx\?fundCode=(?P<id>%(TEXT)s)&amp;univ=T&amp;pagetype=performance&amp;typeCode=%(TEXT)s" class="CustomChartIcon" alt="chart"></a></td><td class=""><a href="/Factsheets/Factsheet.aspx\?fundCode=%(TEXT)s&amp;univ=T">(?P<name>%(TEXT)s)</a></td><td class="Left"><a href="/Factsheets/GroupFactSheet.aspx\?managerCode=%(TEXT)s&amp;univ=T">%(TEXT)s</a></td><td class="" align="center"><a class="%(TEXT)s" title="%(TEXT)s"></a></td><td class="Center">%(NUM)s(?:<br />\(%(TEXT)s\))?</td><td class="Center">%(NUM)s</td><td class="Center">(?P<discount>%(NUM)s)</td><td class="Center">%(NUM)s</td><td class="" align="center"><div id="div%(TEXT)s" class="riskscore_icon" onclick="javascript:OpenRSChart\('%(TEXT)s','%(TEXT)s','%(TEXT)s',this.id,'%(TEXT)s'\);"><b>%(TEXT)s</b></div></td><td class="Center">(?:<a class="FCRcrown%(NUM)s" title="(?P<fecrown>%(NUM)s)"></a>|%(TEXT)s)</td><td class="noWrapCellRight">(?P<m1>%(NUM)s)</td><td class="noWrapCellRight">(?P<m3>%(NUM)s)</td><td class="noWrapCellRight">(?P<m6>%(NUM)s)</td><td class="noWrapCellRight">(?P<y1>%(NUM)s)</td><td class="noWrapCellRight">(?P<y2>%(NUM)s)</td><td class="noWrapCellRight">(?P<y3>%(NUM)s)</td><td class="noWrapCellRight">(?P<y5>%(NUM)s)</td><td class="noWrapCellRight">(?P<y10>%(NUM)s)</td><td class="noWrapCellRight">(?P<m12to24>%(NUM)s)</td><td class="noWrapCellRight">(?P<m24to36>%(NUM)s)</td><td class="noWrapCellRight">(?P<m36to48>%(NUM)s)</td><td class="noWrapCellRight">(?P<m48to60>%(NUM)s)</td><td class="noWrapCellRight">(?P<m1n>%(NUM)s)</td><td class="noWrapCellRight">(?P<m3n>%(NUM)s)</td><td class="noWrapCellRight">(?P<m6n>%(NUM)s)</td><td class="noWrapCellRight">(?P<y1n>%(NUM)s)</td><td class="noWrapCellRight">(?P<y3n>%(NUM)s)</td><td class="noWrapCellRight">(?P<y5n>%(NUM)s)</td><td class="Center"><a href="javascript\:AddFundtoNewBasket\('%(TEXT)s','T',true\);" class="basket_add_icon"></a></td>%(TEXT)s</tr>""" % {'NUM':NUM, 'TEXT':TEXT}
    
    FUND_RE = re.compile(fundReText, re.DOTALL)
    
    def criteria(self, fundMatch):
        criteria_id2value = {
            'm1': 2,
            'm3': 3,
            'm6': 4,
            'y1': 5,
            'y2': 42,
            'y3': 6,
            'y5': 7,
            'y10': 44,
            'm1n': 18,            
            'm3n': 19,            
            'm6n': 20,            
            'y1n': 21,            
            'y3n': 22,            
            'y5n': 23,            
            'm12to24': 54,
            'm24to36': 57,
            'm36to48': 58,
            'm48to60': 59,
            'discount': 15,
            'fecrown': 49,
            }
        # Update or insert fund name
        fund_name = fundMatch['name'].replace('&amp;', '&')
        fund_id = 'it_'+fundMatch['id']
        table = sa.Table('tblFund', metadata, autoload=True)
        if not table.select(table.c.FundId==fund_id).execute().fetchall():
            table.insert().execute(FundId=fund_id, FundName=fund_name)
        else:
            table.update(table.c.FundId==fund_id).execute(FundName=fund_name)
        # Add entries if not present, no deletions
        today = time.strftime('%d-%b-%Y')        
        table = sa.Table('tblFundStatistics', metadata, autoload=True)
        if not table.select(sa.and_(table.c.DateTimeStamp==today,table.c.FundId==fund_id)).execute().fetchall():
            to_insert = []
            for name, value in criteria_id2value.items():
                to_insert.append({
                    'DateTimeStamp': today,
                    'FundId': fund_id,
                    'CriteriaId': int(value),
                    'CriteriaValue': fundMatch[name]})
            if to_insert:
                table.insert().execute(*to_insert)
        return False
    
        
class Citywire(DataSourceBase):

    fundReText = """href="/manager/%(TEXT)s/%(TEXT)s\?section=money">(?P<manager>.+?)</a>.+?</td>.+?ratings/medium__(?P<rating>A+).png.+?</td><td>(?P<fundtext>.+?)</td>""" % {'NUM':NUM, 'TEXT':TEXT}
    
    MANAGER_RE = re.compile(fundReText, re.DOTALL)
    
    fundReText = """<a.+?/fund/%(TEXT)s/(?P<fundid>%(TEXT)s)\?section=money">(?P<fundname>.+?)</a>""" % {'NUM':NUM, 'TEXT':TEXT}
    
    FUND_RE = re.compile(fundReText, re.DOTALL)

    CW_CRITERIA_ID = 25
    BASE_URL = 'http://citywire.co.uk/money/fund-and-fund-manager-performance/all-rated-managers.aspx'
    SOUP_INFO = None
    NUM_PAGES = 1

    def findallmatches(self, compiledRe, text, quickcheck=None):
        matches = []
        while text:
            if quickcheck:                       
                if sum(map(lambda n: text.find(n) != -1, quickcheck)) == 0:
                    return matches
            match = compiledRe.search(text)
            if match:
                matches.append(match)
            else:
                return matches
            end = match.end()
            text = text[end:]
        return matches
        
    def doWork(self):
        # Write status file
        setUpdateStatus(self, 'failed')
        
        # Get the data
        text = self.get_raw_pages()[0]

        # Parse the results page to get the data we really want
        # This will give us all funds for managers with a AAA rating
        results = []
        manager_matches = self.findallmatches(self.MANAGER_RE, text, quickcheck=("medium__A",))

        
        for match in manager_matches:
            row =  match.groupdict()
            result = {
                'manager': row['manager'],
                'rating': len(row['rating'])}
            result['funds'] = []
            fund_matches = self.FUND_RE.finditer(row['fundtext'])
            for fund_match in fund_matches:
                result['funds'].append((
                    fund_match.groupdict()['fundid'],
                    fund_match.groupdict()['fundname']))
            results.append(result)
            
        # Return list of funds alphabetically
        table = sa.Table('tblCitywireFund', metadata, autoload=True) 
        table.update().execute(Rating=0)  # Set all rating to 0
        if len(results) < 2:
            raise ValueError
        for result in results:
            # We only ever update funds.  The effect of this is that
            # if funds drop out of the list, we still remember the
            # name, manager, and consolidation info
            for fund_id, fund_name in result['funds']:
                if not table.select(table.c.CitywireFundId==fund_id).execute().fetchall():
                    table.insert().execute(
                        CitywireFundId=fund_id,
                        CitywireFundName=fund_name,
                        Manager=result['manager'],
                        Rating=result['rating'],
                        Consolidated=0)
                else:
                    table.update(table.c.CitywireFundId==fund_id).execute(
                        CitywireFundName=fund_name,
                        Manager=result['manager'],
                        Rating=result['rating'])

        # Add entries if not present, no deletions
        today = time.strftime('%d-%b-%Y')        
        table = sa.Table('tblFundStatistics', metadata, autoload=True)
        # Remove data for this date and criteriaId
        table.delete(sa.and_(table.c.DateTimeStamp==today,table.c.CriteriaId==self.CW_CRITERIA_ID)).execute()
        # Get the data to add
        data = db.execute("""
            SELECT     tblFund.FundId, tblCitywireFund.Rating
            FROM         tblCitywireFund INNER JOIN
                                  tblFund ON tblCitywireFund.CitywireFundId = tblFund.CitywireFundId        
            """).fetchall()    
        to_insert = []
        for row in data:
            to_insert.append({
                'DateTimeStamp': today,
                'FundId': row['FundId'],
                'CriteriaId': self.CW_CRITERIA_ID,
                'CriteriaValue': row['Rating']})
        if to_insert:
            table.insert().execute(*to_insert)

        # Write status file
        setUpdateStatus(self, 'successful')
        
        
class Wealth150(DataSourceBase):

    fundReText = """<a href="http\://www\.hl\.co\.uk/funds/fund-discounts,-prices--and--factsheets/search-results/(?P<fund_id>%(TEXT)s)" title="%(TEXT)s" class="link-headline">(<strong>)?(?P<fund_name>%(TEXT)s)(</strong>)?</a>""" % {'NUM':NUM, 'TEXT':TEXT}
    
    # print fundReText
    
    FUND_RE = re.compile(fundReText, re.DOTALL)
   
    HL_CRITERIA_ID = 26
    BASE_URL = ('http://www.hl.co.uk/funds/help-choosing-funds/wealth-150?SQ_DESIGN_NAME=blank&f_w150p=false')
    SOUP_INFO = {}
    NUM_PAGES = 1

    def getData(self):
        pages = self.get_raw_pages()

        # Get the data from the h-l website
        # We need to go to a number of URLS
        combined_results = []
        for data in pages:
            # Parse the results page to get the fund names
            results = [match.groupdict() for match in self.FUND_RE.finditer(data)]
            if len(results) < 2:
                raise ValueError
            combined_results.extend(results)
        return combined_results
    
    def doWork(self):
        # Write status file
        setUpdateStatus(self, 'failed')
        
        # Get the data 
        pages = self.get_raw_pages()

        # Get the data from the h-l website
        # We need to go to a number of URLS
        combined_results = []
        for data in pages:
            # Parse the results page to get the fund names
            results = [match.groupdict() for match in self.FUND_RE.finditer(data)]
            if len(results) < 2:
                raise ValueError
            combined_results.extend(results)
        
        # De-duplicate
        new_combined_results = {}
        for item in combined_results:
            if item['fund_name'] in new_combined_results:
                new_combined_results[item['fund_name']] = max(item['fund_id'], new_combined_results[item['fund_name']])
            else:
                new_combined_results[item['fund_name']] = item['fund_id']   
            
        # Return list of funds alphabetically
        table = sa.Table('tblHLFund', metadata, autoload=True)
        table.update().execute(Active=0)  # Set all active to 0        
        for fund_name, fund_id in new_combined_results.items():
            fund_name = fund_name.replace('&amp;', '&')
            # We only ever update funds
            if not table.select(table.c.HLFundId==fund_id).execute().fetchall():
                table.insert().execute(
                    HLFundId=fund_id,
                    HLFundName=fund_name,
                    Active=1,
                    Consolidated=0)
            else:
                table.update(table.c.HLFundId==fund_id).execute(
                    HLFundName=fund_name,
                    Active=1)
                
        # Add entries if not present, no deletions
        today = time.strftime('%d-%b-%Y')        
        table = sa.Table('tblFundStatistics', metadata, autoload=True)
        # Remove data for this date and criteriaId
        table.delete(sa.and_(table.c.DateTimeStamp==today,table.c.CriteriaId==self.HL_CRITERIA_ID)).execute()
        # Get the data to add
        data = db.execute("""
            SELECT     tblFund.FundId, tblHLFund.Active
            FROM         tblHLFund INNER JOIN
                                  tblFund ON tblHLFund.HLFundId = tblFund.HLFundId        
            """).fetchall()    
        to_insert = []
        for row in data:
            to_insert.append({
                'DateTimeStamp': today,
                'FundId': row['FundId'],
                'CriteriaId': self.HL_CRITERIA_ID,
                'CriteriaValue': row['Active']})
        if to_insert:
            table.insert().execute(*to_insert)
            
        # Write status file
        setUpdateStatus(self, 'successful')


if __name__ == '__main__':    
    TrustNet_ALL().doWork()
    TrustNet_IT().doWork()
    Citywire().doWork()
    Wealth150().doWork()
    