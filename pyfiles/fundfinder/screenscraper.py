import cookielib
import Cookie
import cPickle
import os
import os.path
import re
import sets
import time
import urllib
import urllib2

from BeautifulSoup import BeautifulSoup

from fundfinder import config


class ScreenScraper:
    debug = 1
    
    def __init__(self, base_url=None, num_pages=0, soup_info=None, cookie_info=None):
        self.pages = {}
        self.num_pages = num_pages
        self.curr_page = 1
        
        self.base_url = base_url
        self.cookie_info = cookie_info        
        self.default_headers = {'User-agent' : 'Mozilla/4.0 (compatible; MSIE 5.5; Windows NT)'}
        
        self.urlopen = urllib2.urlopen
        self.Request = urllib2.Request
        
        # Install cookie handler and setup initial cookies
        if self.cookie_info:
            self._install_cookie_handler()
            self._setup_cookies()
        
        # Store soup instructions
        self.soup_info = soup_info
        
    def _install_cookie_handler(self):
        self.cj = cookielib.LWPCookieJar()

        # Now we need to get our Cookie Jar installed in the opener 
        # for fetching URLs

        # Get the HTTPCookieProcessor and install the opener in urllib2
        opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(self.cj))
        urllib2.install_opener(opener)
        
    def _setup_cookies(self):
        # if we were making a POST type request,
        # we could encode a dictionary of values here,
        # using urllib.urlencode(somedict)
        txdata = None

        try:
            # create a request object
            self.req = self.Request(
                self.base_url%self.curr_page, 
                txdata, 
                self.default_headers)

            # and open it to return a handle on the url
            handle = self.urlopen(self.req)

        except IOError, e:
            print 'We failed to open "%s".' % theurl
            if hasattr(e, 'code'):
                print 'We failed with error code - %s.' % e.code
            elif hasattr(e, 'reason'):
                print "The error object has the following 'reason' attribute :"
                print e.reason
                print "This usually means the server doesn't exist,",
                print "is down, or we don't have an internet connection."
            sys.exit()

        else:
            if self.debug:
                print 'Here are the headers of the page :'
                print handle.info()
                # handle.read() returns the page
                # handle.geturl() returns the true url of the page fetched
                # (in case urlopen has followed any redirects, which it sometimes does)

        if self.debug:
            print
            print 'These are the cookies we have received so far :'
            
        if self.cookie_info:
            for index, cookie in enumerate(self.cj):
                # Modify any cookies here
                if cookie.name in self.cookie_info:
                    cookie.value = self.cookie_info[cookie.name]
                    
                if self.debug:
                    print index, '  :  ', cookie, cookie.expires                
        
    def next(self):
        """Get the next page."""
        
        # If this is one of many pages substitute in the page no. here
        url = self.base_url
        if self.num_pages > 1:
            url = self.base_url % self.curr_page
            
        if self.debug:
            print url
            
        self.req = self.Request(url, '', self.default_headers)
        handle = self.urlopen(self.req)
        s = handle.read()
        
        if self.soup_info:
            s = BeautifulSoup(s)
            if 'data_element' in self.soup_info:
                element_info = self.soup_info['data_element']
                s = str(s.find(element_info['name'], attrs=element_info['attrs']))

        # import pdb
        # pdb.set_trace()                
        
        self.pages[self.curr_page] = s
        self.curr_page += 1
        