from np.tal.utils import fill_template
from fundfinder import config
import sqlalchemy as sa


db = None
metadata = None


def fill_page(name, subs={}):
    return fill_template(r'%s\%s.html' % (config.TEMPLATES, name), subs)

def get_db():
    global db
    if not db:
        db = sa.create_engine(config.CONSTRING)
    return db

def get_metadata():
    global metadata
    if not metadata:
        metadata = sa.MetaData(get_db())
    return metadata
                         