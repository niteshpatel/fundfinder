from mod_python import apache
from fundfinder import config
from fundfinder import utils
import sqlalchemy as sa


db = utils.get_db()
metadata = utils.get_metadata()


def authenhandler(req):
    # Authentication based on tblUser
    pw = req.get_basic_auth_pw()    
    table = sa.Table('tblUser', metadata, autoload=True)
    userId = table.select(sa.and_(
        table.c.UserName==req.user,
        table.c.Password==req.get_basic_auth_pw())).execute().fetchone()
    if userId:
        req.userId = userId['UserId']
        # Restrict access to certain pages
        if req.uri == '/criteria_management' and req.userId != 1:
            return apache.HTTP_UNAUTHORIZED        
        return apache.OK
    else:
        return apache.HTTP_UNAUTHORIZED
    